// Splash Screen construction and drawing

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.AlphaComposite;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JWindow;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Splash {
	
    public Splash( ) {
    	try {
    		JWindow splashWindow = new JWindow( );

    		splashWindow.setAlwaysOnTop( true );
    		splashWindow.setBackground( new Color( 0, 0, 0, 0 ) );
    		splashWindow.setContentPane( new SplashWindow( ) );
    		splashWindow.getContentPane( ).add( new JLabel( new ImageIcon( ImageIO.read( getClass( ).getResource( "/Splash/WaWaSplash.png" ) ) ) ) );
    		splashWindow.pack( );
    		splashWindow.setLocationRelativeTo( null );
    		splashWindow.setVisible( true );
            
            try {
    		    Thread.sleep( 2000 );
    		} catch(InterruptedException ex) {
    		    Thread.currentThread( ).interrupt( );
    		}
    		
            splashWindow.dispose( );
            } catch ( IOException ex ) {
            	ex.printStackTrace( );
            	}
    	}

    public class SplashWindow extends JPanel {

        public SplashWindow( ) {
            setOpaque( false );
        }
        
        @Override
        protected void paintComponent( Graphics g ) {
            super.paintComponent( g ); 
            Graphics2D g2d = ( Graphics2D ) g.create( );
            g2d.setComposite( AlphaComposite.SrcOver.derive( 0.00f ) );
            g2d.fillRect( 0, 0, getWidth( ), getHeight( ) );
        }     
    }
}