// AudioPlayer features and functionality

import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import javazoom.jlgui.basicplayer.BasicController;
import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerEvent;
import javazoom.jlgui.basicplayer.BasicPlayerException;
import javazoom.jlgui.basicplayer.BasicPlayerListener;

public class AudioPlayer extends BasicPlayer {

	// Playlist and variables
	private static AudioPlayer instance = null;
	private ArrayList< Song > playlist = new ArrayList< Song >( );
	private int index = 0;
	private int lastIndex = 0;
	private String currentSong = "";
	private String lastSong = "";
	private boolean paused = true;
	private boolean opened = false;
	private boolean isSeeking = false;

	// Audio Properties
	private float audioDurationSec = 0;
	private int audioFrameSize = 0;
	private float audioFrameRate = 0;

	// Seek location information
	private byte[ ] cpcmdata;
	private long csms = 0;
	private int lastSeekMs = 0;

	private AudioPlayer( ) {
		super( );
		this.addBasicPlayerListener( new BasicPlayerListener( ) {
			@Override
			public void stateUpdated( BasicPlayerEvent event ) {
				if ( event.getCode( ) == BasicPlayerEvent.EOM ) {
					lastSeekMs = 0;
					paused = true;
					opened = false;
					log( "EOM event caught, player reset." );
				}
				if ( event.getCode( ) == BasicPlayerEvent.SEEKING )
					isSeeking = true;
				if ( event.getCode( ) == BasicPlayerEvent.SEEKED )
					isSeeking = false;
			}

			@Override
			public void progress( int bytesread, long ms, byte[ ] pcmdata, @SuppressWarnings( "rawtypes" ) Map properties ) {
				csms = ms;
				cpcmdata = pcmdata;
			}

			@Override
			public void opened( Object stream, Map properties ) {
				log( "Open event caught" );
				Object[ ] e = properties.entrySet( ).toArray( );
				Object[ ] k = properties.keySet( ).toArray( );
				String line = "Stream properties:";
				for ( int i = 0; i < properties.size( ); i++ ) {
					line += "\n\t" + k[ i ] + ":" + e[ i ];
				}
				log( line );

				File file = new File( ( playlist.get( index ) ).getPath( ) );
				long audioFileLength = file.length( );
				int frameSize = ( int ) properties.get( "mp3.framesize.bytes" );
				float frameRate = ( float ) properties.get( "mp3.framerate.fps" );
				audioFrameSize = frameSize;
				audioFrameRate = frameRate;
				audioDurationSec = ( audioFileLength / ( frameSize * frameRate ) );
				log( "\tframesize " + frameSize + " framerate " + frameRate );
				log( "\tAudio File length in seconds is: " + audioDurationSec );
			}

			@Override
			public void setController( BasicController arg0 ) {
		
			}
		});
	}

	public static AudioPlayer getInstance( ) {
		if( instance == null ) {
			instance = new AudioPlayer( );
		}
		return instance;
	}

	@Override
	public void play( ) throws BasicPlayerException {
	        if( playlist.size( ) == 0 ) {
	                return;
	        }
	        if( !paused || !opened ) {
	        	File f = new File( playlist.get( index ).getPath( ) );
	            log( "Opening file... " + f.getAbsolutePath( ) );
	            open( f );
	            opened = true;
	            super.play( );
	        }
	        if( paused && currentSong == lastSong ) {
	        	super.resume( );
	        }
	        else if( paused && currentSong != lastSong ) {
	        	File f = new File( playlist.get( index ).getPath( ) );
                log( "Opening file... " + f.getAbsolutePath( ) );
                open( f );
                opened = true;
                super.play( );
	        }
	        paused = false;
	}

	@Override
	public void pause( ) throws BasicPlayerException {
		log( "Paused" );
		paused = true;
		super.pause( );
	}

	@Override
	public void stop( ) throws BasicPlayerException {
		paused = false;
		super.stop( );
	}

	public boolean isPaused( ) {
		return paused;
	}

	public boolean isOpenFile( ) {
		return opened;
	}

	public ArrayList< Song > getPlaylist( ) {
		return playlist;
	}

	public int getIndexSong( ) {
		return index;
	}

	public void setIndexSong( int indexParam ) {
        lastSong = playlist.get( lastIndex ).getPath( );
        currentSong = playlist.get( indexParam ).getPath( );
        this.index = indexParam;
        lastSeekMs = 0;
	}

	public boolean isSeeking( ) {
		return isSeeking;
	}

	// Goes to the next song in play list and plays it
	public void nextSong( ) throws BasicPlayerException {
		if ( playlist.size( ) == 0)
			return;
		lastSeekMs = 0;
		paused = false;
		index = ( index + 1 ) % playlist.size( );
		play( );
	}

	// Goes to the previous song and plays it
	public void prvSong( ) throws BasicPlayerException {
		if ( playlist.size( ) == 0 )
			return;
		lastSeekMs = 0;
		paused = false;
		index = ( index - 1 ) % playlist.size( );
		play( );
	}

	// Adds a song to the playlist
	public void addSong( Song nSong ) {
		playlist.add( nSong );
	}

	//Remove a song by index
	public void removeSong( int index ) {
		playlist.remove( index );
	}

	//Remove a song by songPath
	public void removeSong( String songPath ) {
		playlist.remove( songPath );
	}

	public byte[ ] getPcmData( ) {
		return cpcmdata;
	}

	public long getProgressMicroseconds( ) {
		return csms + lastSeekMs;
	}

	public float getAudioDurationSeconds( ) {
		return audioDurationSec;
	}

	public float getAudioFrameRate( ) {
		return audioFrameRate;
	}

	public float getAudioFrameSize( ) {
		return audioFrameSize;
	}

	//Remembers what's the last position relative to the playing song when seeking
	public void setLastSeekPositionInMs( int seekMs ) {
		lastSeekMs = seekMs;
	}

	//For logging
	private void log( String line ) {
		System.out.println( "AudioPlayer] " + line );
		MainView.stf.addText( "AudioPlayer] " + line );
	}
}