// Main program driver

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Point;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JSlider;
import javax.swing.JPopupMenu;
import java.io.File;
import Utils.BackgroundExecutor;
import Utils.Utils;
import javazoom.jlgui.basicplayer.BasicController;
import javazoom.jlgui.basicplayer.BasicPlayerEvent;
import javazoom.jlgui.basicplayer.BasicPlayerException;
import javazoom.jlgui.basicplayer.BasicPlayerListener;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class MainView extends JFrame{
	
	public MainView( ) {
		setForeground( Color.LIGHT_GRAY );
		setBackground( Color.DARK_GRAY );
		this.init( );
		this.uiBehaviour( );
		this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
	}
	
	DefaultListModel< String > songList = new DefaultListModel< String >( );
	ScheduledExecutorService timersExec = Executors.newSingleThreadScheduledExecutor( );	
	ScheduledExecutorService titleExec 	= Executors.newSingleThreadScheduledExecutor( );
	float currentAudioDurationSec 		= 0; 
	
	// AudioPlayer
	AudioPlayer player = AudioPlayer.getInstance( );
	
	// Components
	JPanel container 		  = new JPanel( );
	JButton btnPlay 		  = new JButton( );
	JButton btnAdd 			  = new JButton( );
	JButton btnNext 		  = new JButton( );
	JButton btnPrev 		  = new JButton( );
	JButton btnDel 			  = new JButton( );
	JButton btnDelAll		  = new JButton( );
	JLabel lblplaying 		  = new JLabel( );
	JLabel lblst 			  = new JLabel( );
	JLabel lblet			  = new JLabel( );
	JList< String > jSongList = new JList< String >( songList );
	JPanel panelNP 			  = new JPanel( );
	JPanel contSlbl 		  = new JPanel( );
	SeekBar seekbar	 		  = new SeekBar( );
	FileChooser fc 	 		  = new FileChooser( );
	String audioPath 		  = "";
	
	// Frames
	public static StatusFrame stf = new StatusFrame( );
	
	// Icons
	ImageIcon playIcon 	   = new ImageIcon( getClass( ).getResource( "/Icons/Icon(play).gif" ) );
	ImageIcon pauseIcon    = new ImageIcon( getClass( ).getResource( "/Icons/Icon(pause).gif" ) );
	ImageIcon previousIcon = new ImageIcon( getClass( ).getResource( "/Icons/Icon(prev).gif" ) );
	ImageIcon nextIcon 	   = new ImageIcon( getClass( ).getResource( "/Icons/Icon(next).gif" ) );
	ImageIcon frameIcon    = new ImageIcon( getClass( ).getResource( "/Icons/FrameIcon.png" ) );
	
	final static int MAXLblPChar = 36;
	
	private final JButton menuButton 			  = new JButton( "^" );
	private final JPopupMenu popMenu 			  = new JPopupMenu( );
	private final JCheckBoxMenuItem checkFrame 	  = new JCheckBoxMenuItem( "Frame" );
	private final JCheckBoxMenuItem checkPlaylist = new JCheckBoxMenuItem( "Playlist" );
	private final JCheckBoxMenuItem checkData 	  = new JCheckBoxMenuItem( "Data" );
	private final JCheckBoxMenuItem checkSeekBar  = new JCheckBoxMenuItem( "Seek Bar" );
	private final JCheckBoxMenuItem checkLog 	  = new JCheckBoxMenuItem( "Log" );
	private final JMenuItem mntmExit 			  = new JMenuItem( "Exit" );
	private final JMenu volumeSlider 			  = new JMenu( "Volume" );
	private final JFrame playlistFrame 			  = new JFrame( "Playlist" );
	
	// Initialize Main Window
	private void init( ) {
		setSize( 491,149 );
		setIconImage( frameIcon.getImage( ) );
		setTitle( "HotWaWa" );
		setLocationRelativeTo( null );
		setDefaultCloseOperation( EXIT_ON_CLOSE );
		setResizable( false );
		container.setBackground( Color.DARK_GRAY );
		container.setLayout( null );
		getContentPane( ).add( container );
		
		// Buttons
		int btn_h = 60;
		JPanel contBtns = new JPanel( );
		contBtns.setBackground( Color.DARK_GRAY );
		contBtns.setBounds( 123, 0, 240, btn_h );
		
		// Previous Button
		btnPrev.setIcon( previousIcon );
		btnPrev.setBounds( 0, 0, 20, btn_h );
		
		// Play Button;
		btnPlay.setIcon( playIcon );
		btnPlay.setMnemonic( KeyEvent.VK_SPACE );
		btnPlay.setBounds( 0, 0, 20, btn_h );
		
		// Next Button
		btnNext.setIcon( nextIcon );
		btnNext.setBounds( 0, 0, 20, btn_h );
		
		// Buttons being added to main container panel
		contBtns.add ( btnPrev );
		contBtns.add ( btnPlay );
		contBtns.add ( btnNext );
		container.add( contBtns );
		
		// Now Playing Panel (panelNP)
		panelNP.setLayout( new BoxLayout( panelNP, BoxLayout.PAGE_AXIS ) );
		panelNP.setToolTipText( "Now Playing" );
		panelNP.setBorder( BorderFactory.createMatteBorder( 1, 0, 2, 0, Color.gray ) );
		panelNP.setBounds( 12, 95, 458, 20 );
		panelNP.setBackground( Color.DARK_GRAY );
		lblplaying.setForeground( Color.LIGHT_GRAY );
		
		lblplaying.setText( "Now Playing: " );
		lblplaying.setBounds( 5, 0, 100, 40 );
		
		panelNP.add( lblplaying );
		container.add( panelNP );
		seekbar.setForeground( Color.GREEN );
		seekbar.setBackground( Color.DARK_GRAY );
		
		// SeekBar
		seekbar.setBounds( 12, 81, 458, 10 );
		container.add( seekbar );
		contSlbl.setBackground( Color.DARK_GRAY );
		contSlbl.setForeground( Color.LIGHT_GRAY );
		
		// Labels song time
		contSlbl.setBounds( 12, 60, 458, 20 );
		lblst.setForeground( Color.LIGHT_GRAY );
		contSlbl.add( lblst );
		lblet.setForeground( Color.LIGHT_GRAY );
		contSlbl.add( lblet );
		lblst.setText( "00:00" );
		lblst.setBorder( new EmptyBorder( 0, 0, 0, 200 ) );
		lblet.setText( "00:00" );
		container.add( contSlbl );
		menuButton.setToolTipText( "player options" );

		// Playlist pane
		playlistFrame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		playlistFrame.setSize( 500, 320 );
		playlistFrame.setLocationRelativeTo( null );
		playlistFrame.setResizable( false );
		playlistFrame.getContentPane( ).setLayout( null );
		playlistFrame.setBackground( Color.DARK_GRAY );					
		
		// Playlist container
		JPanel playlistContainer  = new JPanel( );
		playlistFrame.setContentPane( playlistContainer );
		playlistContainer.setBackground( Color.DARK_GRAY );
		playlistContainer.setLayout( null );
		
		int h_list = 100;
		int line1  = 80;
		int line2 = line1 + h_list + 50;
		int _W 	   = 400;
		
		// Playlist button container
		JPanel contBtns2 = new JPanel( );
		contBtns2.setBackground( Color.DARK_GRAY );
		contBtns2.setBounds( 0, line2 + 20, 220, 50 );
		
		// Add buttons to container panel
		playlistContainer.add( btnAdd );
		btnAdd.setText( "Add Song" );
		btnAdd.setBounds( 12, 248, 230, 30 );
		playlistContainer.add( btnDel );
		btnDel.setBounds( 254, 248, 230, 30 );
		btnDel.setText( "Remove Selected Song" );
				
		// Add button container to playlist container
		playlistContainer.add( contBtns2 );
		
		// SongList
		jSongList.setBounds( 12, 12, 472, 224 );
		jSongList.setModel( songList );
		jSongList.setSelectionMode( ListSelectionModel.MULTIPLE_INTERVAL_SELECTION );
		jSongList.setLayoutOrientation( JList.VERTICAL );
		
		// Create ListScroller and add songList to it
		JScrollPane listScroller = new JScrollPane( jSongList );
		listScroller.setBounds( 12, 12, 471, 224 );
		listScroller.setPreferredSize( new Dimension( _W - 10, h_list ) );
		listScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		listScroller.getViewport().setBackground( Color.DARK_GRAY );
		playlistContainer.add( listScroller );
		
		// Playlist double-click song event
		jSongList.addMouseListener( new MouseAdapter( ) {
			public void mouseClicked( MouseEvent evt ) {
				JList list = ( JList )evt.getSource( );
				if( evt.getClickCount( ) == 2 ) {
					log( "Double click detected, playing selected item." );
					int index = list.locationToIndex( evt.getPoint( ) );
					player.setIndexSong( index );
					try {
						player.play( );
					} catch ( BasicPlayerException ev ) {
						ev.printStackTrace( );
					}
				}
			}
		} );
		
		// Add file to playlist button behavior
		btnAdd.addActionListener( new ActionListener( ) {	
			@Override
			public void actionPerformed( ActionEvent e ) {
				int returnVal = fc.showOpenDialog( btnAdd );
				if( returnVal == JFileChooser.APPROVE_OPTION ) {
					File[ ] files = fc.getSelectedFiles( );
					for( File f : files ) {
						Song song = new Song( f.getAbsolutePath( ) );
						player.addSong( song );
						songList.addElement( song.getTitle( ) );
						log( "Added file " + song.getTitle( ) + " to playlist" );
					}
				}
				else {
					log( "No file selected" );
				}
			}
		} );
		
		// Delete button behavior
		btnDel.addActionListener( new ActionListener( ) {
			@Override
			public void actionPerformed( ActionEvent e ) {
				// Executed Outside UI Thread
				BackgroundExecutor.get( ).execute( new Runnable( ) {
					@Override
					public void run( ) {
						int[ ] indexes = jSongList.getSelectedIndices( );
						int removed = 0;
						for( int i : indexes ) {
							log( "Removed Song (" + ( i - removed ) + ")" + songList.get( i - removed ) );
							player.removeSong( i - removed );
							songList.remove( i - removed );
							removed++;
						}
					}
				} );
			}
		} );
		
		
		// Menu Button
		menuButton.setBounds( 437, 0, 44, 20 );
		container.add( menuButton );
		popMenu.setPopupSize( new Dimension( 80, 80 ) );
		
		// SubMenu
		addPopup( menuButton, popMenu );
		popMenu.add( volumeSlider );
		
		// SubMenu - Volume
		JSlider slider = new JSlider( );
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				float v = (float)slider.getValue()/100;

				/* DEBUG
				System.out.println( "Volume set at " + v );
				// END DEBUG */
				//sets the system volume
				//Audio.setMasterOutputVolume(v);
				
			}
		});
		volumeSlider.add( slider );
			
		// SubMenu - Display
		JMenu displayFlyout = new JMenu( "Display" );
			popMenu.add( displayFlyout );
				checkFrame.setSelected(true);
				displayFlyout.add( checkFrame );
				displayFlyout.add( checkPlaylist );
				checkData.setSelected(true);
				displayFlyout.add( checkData );
				checkSeekBar.setSelected(true);
				displayFlyout.add( checkSeekBar );
				displayFlyout.add( checkLog );			
		
		// SubMenu - Exit
		popMenu.add( mntmExit );
			
		mntmExit.addActionListener( new ActionListener( ) {
			@Override
			public void actionPerformed( ActionEvent evt ) {
				/* DEBUG
				System.out.println( "MouseEvent received. System exiting." );
				//END DEBUG */
				System.exit( 1 );
			}
		} );
	}
	
	private void uiBehaviour( )
	{
		// File chooser
		fc.setMultiSelectionEnabled( true );
		fc.setFileFilter( new FileFilter( ) {
			
			@Override
			public String getDescription( ) {
				return ".mp3";
			}
			
			@Override
			public boolean accept( File f ) {
				if(f.isDirectory( ) )
					return true;
				if(f.getName( ).endsWith( ".mp3" ) )
					return true;
				return false;
			}
		} );		

		checkFrame.addActionListener( new ActionListener( ) {
			public void actionPerformed( ActionEvent evt ) {
				MainView.setDefaultLookAndFeelDecorated( checkFrame.isSelected( ) );
			}
		} );
		
		checkPlaylist.addActionListener( new ActionListener( ) {
			@Override
			public void actionPerformed( ActionEvent evt ) {
				/* DEBUG
				System.out.println( "MouseEvent received. addPlaylist( ) will be invoked." );
				// END DEBUG */
				if( checkPlaylist.isSelected( ) ) {
					playlistFrame.setVisible( true );
				}
				else if( !checkPlaylist.isSelected( ) ) {
					playlistFrame.setVisible( false );
				}
			}
		} );
		
		checkData.addActionListener( new ActionListener( ) {
			@Override
			public void actionPerformed( ActionEvent evt ) {
				/* DEBUG
				System.out.println( "MouseEvent received. panelNP.setVisible will be invoked." );
				// END DEBUG */
				panelNP.setVisible( checkData.isSelected( ) );
			}
		} );
		
		checkSeekBar.addActionListener( new ActionListener( ) {
			@Override
			public void actionPerformed( ActionEvent evt ) {
				/* DEBUG
				System.out.println( "MouseEvent received. seekbar.setVisible will be invoked." );
				// END DEBUG */
				seekbar.setVisible( checkSeekBar.isSelected( ) );
				contSlbl.setVisible( checkSeekBar.isSelected( ) );
			}
		} );
		
		checkLog.addActionListener( new ActionListener( ) {
			@Override
			public void actionPerformed( ActionEvent evt ) {
				/* DEBUG
				System.out.println( "MouseEvent received. checkLog.setVisible will be invoked." );
				// END DEBUG */
				stf.setVisible( !stf.isVisible( ) );				
			}
		});
		
		menuButton.addMouseListener( new MouseAdapter( ) {
			@Override
			public void mouseClicked( MouseEvent e ) {
				/* DEBUG
				System.out.println( "MouseEvent received. menuButton commands will be invoked." );
				// END DEBUG */
				Component b = ( Component )e.getSource( );
				Point p = b.getLocationOnScreen( );
				popMenu.show( b,0,0 );
				popMenu.setLocation( p.x, p.y + b.getHeight( ) );
			}
		} );

		// Play button
		btnPlay.addActionListener( new ActionListener( ) {
			@Override
			public void actionPerformed( ActionEvent evt ) {
				/* DEBUG
				System.out.println( "MouseEvent received. togglePlay will be invoked." );
				// END DEBUG */
				try {
					togglePlay( );
				} catch ( BasicPlayerException e1 ) {
					e1.printStackTrace( );
				}
			}
		} );
		
		// Next button
		btnNext.addActionListener( new ActionListener( ) {
			@Override
			public void actionPerformed( ActionEvent evt ) {
				/* DEBUG
				System.out.println( "MouseEvent received. player.nextSong will be invoked." );
				// END DEBUG */
				try {
					player.nextSong( );
				} catch ( BasicPlayerException e ) {
					log( "Error calling the next song" );
					e.printStackTrace( );
				}
			}
		} );
		
		// Previous button
		btnPrev.addActionListener( new ActionListener( ) {
			@Override
			public void actionPerformed( ActionEvent arg0 ) {
				/* DEBUG
				System.out.println( "MouseEvent received. player.prvSong will be invoked." );
				// END DEBUG */
				try {
					player.prvSong( );
				} catch ( BasicPlayerException e ) {
					log( "Error calling the previous song" );
					e.printStackTrace( );
				}
			}
		} );
		
		// Player related behavior
		player.addBasicPlayerListener( new BasicPlayerListener( ) {
			@Override
			public void stateUpdated( BasicPlayerEvent event ) {
				/* DEBUG
				System.out.println( "MouseEvent received. player.nextSong will be invoked." );
				// END DEBUG */
				if( event.getCode( ) == BasicPlayerEvent.EOM )
				{
					try {
						player.nextSong( );
					} catch ( BasicPlayerException e ) {
						e.printStackTrace( );
					}
					log( "EOM event caught, calling next song." );
				}
				if( event.getCode( ) == BasicPlayerEvent.PAUSED ) {
					btnPlay.setIcon( playIcon );
				}
				if( event.getCode( ) == BasicPlayerEvent.RESUMED ) {
					btnPlay.setIcon( pauseIcon );
				}
			}
			
			@Override
			public void setController( BasicController arg0 ) { }
			
			@Override
			public void progress( int bytesread, long microseconds, byte[ ] pcmdata, Map properties ) {
				seekbar.updateBar( player.getProgressMicroseconds( ), currentAudioDurationSec );
			}
			
			@Override
			public void opened( Object arg0, @SuppressWarnings( "rawtypes" ) Map arg1 ) {
				btnPlay.setIcon( playIcon );
				jSongList.setSelectedIndex( player.getIndexSong( ) );
				lblplaying.setText( "Now Playing: " + songList.get( player.getIndexSong( ) ) );
				currentAudioDurationSec = player.getAudioDurationSeconds( );
			}
		}); //END LISTENER

		//Timer Executor. One second intervals.
		timersExec.scheduleAtFixedRate( new Runnable( ) {
			
			@Override
			public void run( ) {
				updateTimers( );
				//updatePlayingText(); /*TODO: Remove this?*/
			}
		}, 0, 1, TimeUnit.SECONDS );
		
		
		titleExec.scheduleAtFixedRate( new Runnable( ) {
			
			@Override
			public void run( ) {
				updatePlayingText( );
			}
		}, 0, 1, TimeUnit.SECONDS );
	}
	
	// Used by the Play/Pause button
	private void togglePlay( ) throws BasicPlayerException {
		if( songList.size( ) == 0 )
			return;
		if( !player.isPaused( ) ) {
			player.pause( );
			btnPlay.setIcon( playIcon );
			}
		else { player.play( ); }
	}
	
	private void updateTimers( ) {
		if( !player.isPaused( ) ) {
			/*TODO: Remove this?
			//long lms = player.getProgressMicroseconds(); */
			String timer0 = Utils.getMinutesRapp( player.getProgressMicroseconds( ) );
			String timer1 = Utils.getMinutesRapp( ( long )( currentAudioDurationSec*1000000 ) - player.getProgressMicroseconds( ) );
			lblst.setText( timer0 );
			lblet.setText( timer1 );
		}
	}
	
	int dispIndex = 0;
	boolean goback = false;

	private void updatePlayingText( ) {
		if( player.isPaused( ) ) {
			return;
		}
		if( songList == null || ( songList.size( ) == 0 ) ) {
			return;
		}
		String currentSong = songList.get( player.getIndexSong( ) );
		if( currentSong.length( ) > MAXLblPChar ) {
			if( ( MAXLblPChar + dispIndex ) >= currentSong.length( ) )
				goback = true;
			if( dispIndex == 0 )
				goback = false;
			String cutStr = currentSong.substring( dispIndex, MAXLblPChar + dispIndex );
			lblplaying.setText( "Now Playing: " + cutStr );
			if( !goback ) {
				dispIndex++;
			}
			else {
				dispIndex--;
			}
		}
	}
	
	// MAIN
	public static void main( String[ ] args ) {	
		@SuppressWarnings( "unused" )
		Splash splashS = new Splash( );	

		SwingUtilities.invokeLater( new Runnable( ) {
			@Override
			public void run( ) {
				MainView mv = new MainView( );
				mv.setVisible( true );
			}
		} );
	}
	
	private void log( String line )
	{
		System.out.println( "UI-MainView] " + line );
		stf.addText( "UI-MainView] " + line );
	}
	
	private static void addPopup( Component component, final JPopupMenu popup ) { } // Don't listen to your IDE. WE NEED THIS!!!!!
}