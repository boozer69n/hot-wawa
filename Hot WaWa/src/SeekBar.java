// Seekbar functionality and parameters

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JProgressBar;
import Utils.BackgroundExecutor;
import javazoom.jlgui.basicplayer.BasicPlayerException;

public class SeekBar extends JProgressBar {

	AudioPlayer player = AudioPlayer.getInstance();
	private int updateNumber = 0;

	public SeekBar() {
		super();
		setMaximum(10000);
		addMouseListener(new MouseListener() {
			public void mouseReleased(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {
				if(player.isSeeking()) { return; }
				float val = ((float) e.getX() / getWidth()) * getMaximum();
				return_value(val);
				setValue((int)val);
			}
			public void mouseExited(MouseEvent e) {}
			public void mouseEntered(MouseEvent e) {}
			public void mouseClicked(MouseEvent e) {}
		});
	}
	
	public void updateBar(long progress, float total) {
		if(player.isSeeking()) { return; }
		BackgroundExecutor.get().execute(new UpdateThread(progress, total));
		setValue(updateNumber);
	}

	private class UpdateThread implements Runnable {
		long progress;
		float total;
		
		public UpdateThread(long progress, float total) {
			this.progress = progress;
			this.total = total;
		}
		
		public void run() {			
			int i = (int) (progress / 1000);
			int len = getMaximum();
			int j = (int) ((i / (total * 1000)) * len);
			updateNumber = j;
		}
	}

	private void return_value(float value) {
		BackgroundExecutor.get().execute(new SeekThread(value));
	}
	
	class SeekThread implements Runnable {
		
		float value;
		public SeekThread(float value) {
			this.value = value;
		}
		
		public void run() {
			float relative = value / getMaximum();
			float frameRate = player.getAudioFrameRate();
			float frameSize = player.getAudioFrameSize();
			float duration = player.getAudioDurationSeconds();
			int newPosition  = (int) (relative * duration);
			player.setLastSeekPositionInMs(newPosition * 1000000);
			long seekValue = (long) (newPosition  * frameRate * frameSize);
			try {
				player.seek(seekValue);
			} catch (BasicPlayerException e) {
				e.printStackTrace();
			}
		}
	}
}