import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class FileChooser extends JFrame{
	private JTextArea textOutput;	// output text file desired for opening
	private JScrollPane scrollBar;	// scroller to find file(s) in directory
	
	// constructor
	public FileChooser(){
		super();
		analyze();	// creates and analyzes file object
	}
	
	// this method will allow give the user(s) the ability to choose the
	// directory(ies) and/or file(s)
	private File FileDirectory(){
		// prompts user with text, indicating that the user should choose
		// a file or directory
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		
		int result = fileChooser.showOpenDialog(this);
		
		// returns null if user clicks cancel
		if(result == JFileChooser.CANCEL_OPTION){
			System.exit(1);
		}
		
		// retrieves file
		File file = fileChooser.getSelectedFile();
		
		// prompts user with error message if information is invalid
		if((file == null) || (file.getName().equals(""))){
			JOptionPane.showMessageDialog(this, "Invalid Name", "Invalid Name", 
					JOptionPane.ERROR_MESSAGE);
			
		System.exit(1);
		}
		
		return file;
	}
	
	// displays information about the user selected file or directory
	public void analyze(){
		// creates file object
		File file = FileDirectory();
		
		// displays information about the audio file if it exists
		if(file.exists()){
			textOutput.setText(String.format("%s%s\n%s\n%s\n%s\n%s%s\n%s%s\n%s%s\n%s%s\n%s%s",
					file.getName(), " exists", 
					(file.isFile() ? "is an existing file" : "is not an existing file"),
					(file.isDirectory() ? "is an existing directory" : "is not an existing "
							+ "directory"),
					(file.isAbsolute() ? "is an absolute path" : "is not an absolute path"),
					"Last Modified", file.lastModified(), "Length: ", file.length(),
					"Path: ", file.getPath(), "Absolute path: ", file.getAbsolutePath(),
					"Parent: ", file.getParent()));
			
			// lists directory
			if(file.isDirectory()){
				String[] directories = file.list();
				textOutput.append("\n\nDirectory contents:\n");
				
				for(String directoryName : directories){
					textOutput.append(directoryName + "\n");
				}
			}
		}
		else{	// prompts user(s) with an error message if the file(s) or directory() don't exist
			JOptionPane.showMessageDialog(this, file + " does not exist.", "ERROR", 
					JOptionPane.ERROR_MESSAGE);
		}
	}
}
