/**
 * Extracts the meta data from the audio file and stores it for later retrieval
 * ======================================================================================
 * Javadocs
 * 	https://tika.apache.org/1.9/api/
 *	To Add: Right click tika-app-1.9.jar > properties > javadocs location > fill in url
 * ====================================================================================== * 
 * Resources Sites:
 * https://tika.apache.org/1.9/api/
 * https://en.wikipedia.org/wiki/ID3 * 
 * 
 * https://www.youtube.com/watch?v=OrwO_Q4QJNc
 * http://stackoverflow.com/questions/10824027/get-the-metadata-of-a-file
 * http://stackoverflow.com/questions/1645803/how-to-read-mp3-file-tags
 * ======================================================================================
 * ##### Where they are stored within the file and how to call #####
 * 
 * Media
 * 	Genre = xmpDM:genre
 * 	Contributing artist = creator
 * 	Contributing artist = meta:author
 * 	Contributing artist = xmpDM:artist
 * 	Contributing artist = dc:creator
 * 	Contributing artist = Author
 * 	Year = xmpDM:releaseDate
 * 	Album artist = xmpDM:albumArtist
 * 	
 * Content
 * 	Composers = xmpDM:composer
 * 
 * Description
 * 	Title = title
 * 	Comments = xmpDM:logComment //comes out as "eng - comments..."
 * 	Title = dc:title
 * 
 * Unknown
 * 	xmpDM:compilation
 * 
 * From file = xmpDM:audioCompressor //its type like "MP3"
 * 			 = version //it pulls things like "MPEG 3 Layer III Version 1"
 * 			 = xmpDM:audioSampleRate = quality of the audio "44100"
 * 			 = channels //displays if stereo "2"
 * 			 = xmpDM:duration //time in milliseconds "164814.796875"
 * 			 = Content-Type //audio type "audio/mpeg"
 * ======================================================================================
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.apache.tika.parser.ParseContext;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Song{
	private String album;
	private String artist;
	private String genre;
	private String path;
	private String title;
	private String track;
	private double length;
	private String year;
	
	public Song(String path) {
		path = path.replace("\\", File.separator);
		try {
			InputStream input = new FileInputStream(new File(path));
			ContentHandler handler = new DefaultHandler();
			Metadata metadata = new Metadata();
			Parser parser = new Mp3Parser();
			ParseContext parse_context = new ParseContext();
			parser.parse(input, handler, metadata, parse_context);
			input.close();
			
			this.album = metadata.get("xmpDM:album");
			this.artist = metadata.get("xmpDM:artist");
			this.genre = metadata.get("xmpDM:genre");
			this.path = path;
			this.title = metadata.get("title");
			this.length = Double.parseDouble(metadata.get("xmpDM:duration"));
			this.year = metadata.get("xmpDM:releaseDate");
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (TikaException e) {
			e.printStackTrace();
		}
	}
	
	//setter
	public void set_path(String path) { this.path = path; }
	
	//getters
	public String getAlbum() 	{ return album; }
	public String getArtist() 	{ return this.artist; }
	public String getGenre() 	{ return genre; }
	public String getTitle() 	{ return title; }
	public String getTrack() 	{ return track; }
	public String getYear() 	{ return year; }
	public double getLength() 	{ return length; }
	public String getPath() {
		String new_path = this.path.replace("\\", File.separator);
		return new_path;
	}
}