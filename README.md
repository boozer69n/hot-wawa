**Name**
    - Hot WaWa 

**Members**
    - Clinton Booze 
    - Jonathan Gill 
    - Ian Tyler 
    - LeDeryk Pane 

**Project Ideas** 
    - A music player/organizer 

**Interview/Customer** 
    - Dustin Buschow - He is my newly hired boss and is extremely knowledgeable about tech matters and is helping me learn higher levels of IT and systems in general.

**UML Program**
    - Google Slides

**Communication/Collaboration**
    - Hangouts
    - BitBucket
    - Skype
    - Hip-Chat
    - SourceTree
    - E-Mail - *As a last resort*

**Language**
    - Java

**Requirements** 
    - Assignment 2.1 - Define the requirements in detail for your project. Use your textbook as a guide for what should be included in the requirements document. 
    - Assignment 2.2 - Create use case diagrams for your project. Include a minimum of 3 actors, 12 use cases, 2 extends and 2 generalizations 
    - Assignment 3.1 - Create a class diagram for your project. 
    - Assignment 3.2 - After having your customer review your project so far, revise through iteration what needs to change. 
    - Assignment 4.1 - Add 5 interaction diagrams to your project documentation. 
    - Assignment 4.2 - Apply 2 design patterns to the project. 
    - Assignment 4.3 - Have your customer review your project to date. Revise through iteration (based on Agile principles) what needs to change. 
    - Assignment 5.1 - Have your customer review your final product (application and documentation). 
    - Assignment 5.2 - Add a testing plan and at least 5 metrics to your application.