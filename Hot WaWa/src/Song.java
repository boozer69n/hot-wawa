//Extracts the meta data from the audio file and stores it for later retrieval

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.apache.tika.parser.ParseContext;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Song{
	private String album;
	private String artist;
	private String genre;
	private String path;
	private String title;
	private String track;
	private double length;
	private String year;
	
	public Song( String path ) {
		this.path = path;
		try {
			InputStream input = new FileInputStream( new File( path ) );
			ContentHandler handler = new DefaultHandler( );
			Metadata metadata = new Metadata( );
			Parser parser = new Mp3Parser( );
			ParseContext parse_context = new ParseContext( );
			parser.parse( input, handler, metadata, parse_context );
			
			this.album = metadata.get( "xmpDM:album" );
			this.artist = metadata.get( "xmpDM:artist" );
			this.genre = metadata.get( "xmpDM:genre" );
			
			if ( metadata.get( "title" ) == null ) {
				Path p = Paths.get( path );
				this.title = p.getFileName( ).toString( ).substring( 0, p.getFileName( ).toString( ).length( ) - 4 );
			} else {
				this.title = metadata.get( "title" );
			}			
			this.length = Double.parseDouble( metadata.get( "xmpDM:duration" ) );
			this.year = metadata.get( "xmpDM:releaseDate" );
			
		} catch ( FileNotFoundException e ) {
			e.printStackTrace( );
		} catch ( IOException e ) {
			e.printStackTrace( );
		} catch ( SAXException e ) {
			e.printStackTrace();
		} catch ( TikaException e ) {
			e.printStackTrace();
		}
	} // End of song()
	
	public String getAlbum( ) 			{ return album; }
	public String getArtist( ) 			{ return this.artist; }
	public String getGenre( ) 			{ return genre; }
	public String getTitle( ) 			{ return title; }
	public String getTrack( ) 			{ return track; }
	public String getYear( ) 			{ return year; }
	public double getLength( ) 			{ return length; }
	public String getPath( ) 			{ return path; }
	public void set_path( String path ) { this.path = path; }
}