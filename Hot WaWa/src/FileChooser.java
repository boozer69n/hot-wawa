// Allows the user(s) the ability to choose the directory(s) and/or file(s)

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class FileChooser extends JFileChooser{
	private JTextArea textOutput;
	private JScrollPane scrollBar; // Don't listen to your IDE. WE NEED THIS!!!!!

	public FileChooser(){
		super();
	}
	
	private File FileDirectory(){
		
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		
		int result = fileChooser.showOpenDialog(this);
		
		// Returns null if user clicks cancel
		if(result == JFileChooser.CANCEL_OPTION){
			System.exit(1);
		}
		
		// Gets file
		File file = fileChooser.getSelectedFile();
		
		// Error if info is invalid
		if((file == null) || (file.getName().equals(""))){
			JOptionPane.showMessageDialog(this, "Invalid Name", "Invalid Name", 
					JOptionPane.ERROR_MESSAGE);
			
		System.exit(1);
		}
		
		return file;
	}
	
	// Displays information about the user selected file or directory.
	public void analyze(){
		File file = FileDirectory();
		
		// Displays information about the audio file, if it exists.
		if(file.exists()){
			textOutput.setText(String.format("%s%s\n%s\n%s\n%s\n%s%s\n%s%s\n%s%s\n%s%s\n%s%s",
					file.getName(), " exists", 
					(file.isFile() ? "is an existing file" : "is not an existing file"),
					(file.isDirectory() ? "is an existing directory" : "is not an existing "
							+ "directory"),
					(file.isAbsolute() ? "is an absolute path" : "is not an absolute path"),
					"Last Modified", file.lastModified(), "Length: ", file.length(),
					"Path: ", file.getPath(), "Absolute path: ", file.getAbsolutePath(),
					"Parent: ", file.getParent()));
			
			// Lists the directory.
			if(file.isDirectory()){
				String[] directories = file.list();
				textOutput.append("\n\nDirectory contents:\n");
				
				for(String directoryName : directories){
					textOutput.append(directoryName + "\n");
				}
			}
		}
		// Throw an error message if the file(s) or directory() doesn't exist
		else{
			JOptionPane.showMessageDialog(this, file + " does not exist.", "ERROR", 
					JOptionPane.ERROR_MESSAGE);
		}
	}
}